//
//  Routing.swift
//  MVVM with RxSwift
//
//  Created by hripsimem on 11/30/20.
//  Copyright © 2020 hripsimem. All rights reserved.
//

import Foundation

protocol Routing {

    // Base url
    var baseURLString: String { get }
    var method: RequestType { get }
    var routPath: String { get }
    var key: String { get }
    var parameters: [String: Any]? { get }
    var encoding: ParameterEncoding { get }
    var headers: [String: String]? { get }
    var urlRequest: URLRequest? { get }
}

extension Routing {
    var baseURLString: String {
         return "https://next.json-generator.com/api/json/"
    }

    var method: RequestType {
        return .POST
    }

    var routPath: String {
        return ""
    }

    var parameters: [String: Any]? {
        return nil
    }

    var encoding: ParameterEncoding {
        return ParameterEncoding.url
    }

    var headers: [String: String]? {
        return nil
    }

    var urlRequest: URLRequest? {
        let baseURLStirng = baseURLString

        guard var url = URL(string: baseURLStirng) else {
            #if DEV
            print("cannot create URL")
            #endif

            return nil
        }

        if !routPath.isEmpty {
            url.appendPathComponent(routPath)
        }

        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = method.rawValue

        if let headers = self.headers {
            for (key, value) in headers {
                urlRequest.addValue(value, forHTTPHeaderField: key)
            }
        }

        if let parameters = self.parameters {
            do {
                urlRequest = try encoding.encode(request: urlRequest, parameters: parameters)
            } catch {
                #if DEV
                print("parameters encoding issue")
                #endif
            }
        }

        return urlRequest
    }
}


