//
//  NetworkManager.swift
//  MVVM with RxSwift
//
//  Created by hripsimem on 11/30/20.
//  Copyright © 2020 hripsimem. All rights reserved.
//

import Foundation
import RxSwift

class NetworkManager {
    init() {}

    func fetch<R: Routing>(_ routing: R) -> Observable<Data> {
        return Observable.create { observer in

        // set up the session
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)

        // make the request
            if let urlRequest = routing.urlRequest {
                let task = session.dataTask(with: urlRequest) {
                    (data, response, errorResponse) in
                    // check for any errors
                    guard errorResponse == nil else {
                        observer.onError(errorResponse!)
                        return
                    }

                    guard let responseData = data else {
                        print("Data does not exist")

                        return
                    }

                        observer.onNext(responseData)
                        observer.on(.completed)
                }
                task.resume()
            }
            return Disposables.create()
        }
    }
}

