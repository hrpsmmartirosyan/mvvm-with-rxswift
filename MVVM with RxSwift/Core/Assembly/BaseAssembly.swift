//
//  BaseAssembly.swift
//  MVVM with RxSwift
//
//  Created by hripsimem on 12/2/20.
//  Copyright © 2020 hripsimem. All rights reserved.
//

import Foundation
import Swinject

class BaseAssembly: Assembly {
    init() {}

    func assemble(container: Swinject.Container) {}
}

