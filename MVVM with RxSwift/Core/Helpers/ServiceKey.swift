//
//  ServiceKey.swift
//  MVVM with RxSwift
//
//  Created by hripsimem on 12/2/20.
//  Copyright © 2020 hripsimem. All rights reserved.
//

import Foundation

public enum ServiceKey: String {
    case Countries  = "CountryService"
}
