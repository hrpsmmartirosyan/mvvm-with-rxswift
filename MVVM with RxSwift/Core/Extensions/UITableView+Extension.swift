//
//  UITableView+Extension.swift
//  MVVM with RxSwift
//
//  Created by hripsimem on 12/3/20.
//  Copyright © 2020 hripsimem. All rights reserved.
//

import Foundation
import UIKit

extension UITableViewCell: ReusableView {}

extension UITableView {
    public func registerCell<T: UITableViewCell>(with _: T.Type, bundle: Bundle? = nil) {
         let bundle = bundle ?? Bundle(for: T.self)

         let nib = UINib(nibName: T.nibName, bundle: bundle)

         if bundle.path(forResource: T.nibName, ofType: "nib") != nil {
             register(nib, forCellReuseIdentifier: T.reuseIdentifier)
         } else {
             register(T.self, forCellReuseIdentifier: T.reuseIdentifier)
         }
     }
    
    func dequeueReusableCell<T: UITableViewCell>(for indexPath: IndexPath) -> T {
        guard let cell = dequeueReusableCell(withIdentifier: T.reuseIdentifier, for: indexPath) as? T else {
            preconditionFailure("Could not dequeue cell: \(T.self) with identifier: \(T.reuseIdentifier)")
        }

        return cell
    }
}
