//
//  ViewModelled.swift
//  MVVM with RxSwift
//
//  Created by hripsimem on 12/3/20.
//  Copyright © 2020 hripsimem. All rights reserved.
//

import Foundation

public protocol ViewModelled {
    associatedtype T: ViewModelling

    var viewModel: T? { get }

    func bindViewModel(_ viewModel: T)
}

