//
//  ErrorInfo.swift
//  MVVM with RxSwift
//
//  Created by hripsimem on 12/2/20.
//  Copyright © 2020 hripsimem. All rights reserved.
//

import Foundation

public struct ErrorData {
    public let service: String
    public let code: String
    public var serverMessage: String

    public init(service: String, code: String, serverMessage: String) {
        self.service = service
        self.code = code
        self.serverMessage = serverMessage
    }

    public var message: String? {
        return message(for: code, service: service)
    }

    private func message(for code: String, service: String) -> String? {
        guard let serviceKey = ServiceKey(rawValue: service)?.rawValue else { return nil }

        let message = String(serviceKey) + "." + code

        return message
    }
}

