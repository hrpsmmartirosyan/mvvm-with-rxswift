//
//  ErrorResponse.swift
//  MVVM with RxSwift
//
//  Created by hripsimem on 12/2/20.
//  Copyright © 2020 hripsimem. All rights reserved.
//

import Foundation

protocol ServiceErrorHandling {
    func handle(for service: String) -> Error?
}

public final class ErrorResponse: Codable {
    public let statusCode: Int?
    public let errorCode: Int?
    public let message: String

    public init(statusCode: Int?, errorCode: Int?, message: String) {
        self.statusCode = statusCode
        self.message = message
        self.errorCode = errorCode
    }

    private enum CodingKeys: String, CodingKey {
        case statusCode = "status"
        case message = "message"
        case errorCode = "errorCode"
    }

    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        self.statusCode = try? values.decode(Int.self, forKey: .statusCode)
        self.errorCode = try? values.decode(Int.self, forKey: .errorCode)
        self.message = try values.decode(String.self, forKey: .message)
    }
}

extension ErrorResponse: ServiceErrorHandling {
    func handle(for service: String) -> Error? {
        if let errorCode = self.errorCode, errorCode != 0  {
            return ServiceError.functional(service: service, code: String(errorCode), message: self.message)
        }
        return nil
    }
}

