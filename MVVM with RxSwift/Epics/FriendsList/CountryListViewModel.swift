//
//  CountryListViewModel.swift
//  MVVM with RxSwift
//
//  Created by hripsimem on 12/2/20.
//  Copyright © 2020 hripsimem. All rights reserved.
//

import RxSwift
import RxCocoa

final class CountryListInputs {
    let countryListService: CountryListServicing

    public init(countryListService: CountryListServicing) {
        self.countryListService = countryListService
    }
}

final class CountryListViewModel: ViewModel<CountryListInputs> {
    // MARK: Public members
    public var datasource =  BehaviorRelay<[CountryCellPresentationModel]>(value: [])

    //MARK: Public API
    func fetchCountryList() {
        let parameters = CountryListParameters()

        self.inputs.countryListService
            .call(with: parameters)
            .subscribe { [weak self] event in
                guard let `self` = self else { return }

                self.handleEvent(event) { [weak self] list in
                    let countryList = list.countries.map { CountryCellPresentationModel(name: $0.name, description: $0.description) }

                    self?.datasource.accept(countryList)
                }
            }
            .disposed(by: disposeBag)
    }
}



