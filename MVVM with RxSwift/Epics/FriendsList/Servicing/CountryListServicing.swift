//
//  CountryListServicing.swift
//  MVVM with RxSwift
//
//  Created by hripsimem on 12/2/20.
//  Copyright © 2020 hripsimem. All rights reserved.
//

import Foundation
import RxSwift

public protocol CountryListServicing {
    /**
     Get client information
     
     - returns:  returned Country list, or throws error via Observable
     */
    func call(with parameters: CountryListParameters) -> Observable<CountryList>
}
