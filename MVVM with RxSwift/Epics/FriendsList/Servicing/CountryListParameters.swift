//
//  CountryListParameters.swift
//  MVVM with RxSwift
//
//  Created by hripsimem on 12/2/20.
//  Copyright © 2020 hripsimem. All rights reserved.
//

import Foundation

public struct CountryListParameters {
    public init() {}
}

extension CountryListParameters: Routing {
    var key: String {
        return ServiceKey.Countries.rawValue
    }

    var method: RequestType {
        return .GET
    }

    var encoding: ParameterEncoding {
        return .json
    }

    var routPath: String {
        return "get/NkVPk2eoF"
    }
}


