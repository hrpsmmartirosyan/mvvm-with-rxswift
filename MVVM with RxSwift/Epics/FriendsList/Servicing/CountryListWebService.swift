//
//  CountryListWebService.swift
//  MVVM with RxSwift
//
//  Created by hripsimem on 12/3/20.
//  Copyright © 2020 hripsimem. All rights reserved.
//

import Foundation
import RxSwift

class CountryListWebService: DataFetchingManager, CountryListServicing {
    
    func call(with parameters: CountryListParameters) -> Observable<CountryList> {
        return self.execute(parameters, errorType: ErrorResponse.self)
    }
}
