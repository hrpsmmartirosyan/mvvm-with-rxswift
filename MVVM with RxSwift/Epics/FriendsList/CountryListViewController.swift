//
//  CountryListViewController.swift
//  MVVM with RxSwift
//
//  Created by hripsimem on 12/2/20.
//  Copyright © 2020 hripsimem. All rights reserved.
//

import UIKit
import RxSwift

class CountryListViewController: BaseViewController<CountryListViewModel>, UITableViewDelegate, UITableViewDataSource {

    // MARK: Private members
    private var countryLisTableView = UITableView()
    private var activityIndicator: UIActivityIndicatorView!

    // MARK: - View life cycle
    override open func viewDidLoad() {
        super.viewDidLoad()

        self.title = "country.list.title".localized
        self.setupTableView()
        self.viewModel?.fetchCountryList()

        self.setupLoader()
    }

    // MARK: - ViewModel life cycle
    open override func bindViewModel(_ viewModel: CountryListViewModel) {
        viewModel.datasource
            .asDriver()
            .drive(onNext: { [weak self] datasource in
                if !datasource.isEmpty {
                    self?.countryLisTableView.reloadData()
                    self?.showLoading(false)
                }
            })
            .disposed(by: disposeBag)
    }

    // MARK: - TableView dataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let datasourc = self.viewModel?.datasource.value else  { return 0 }

        return datasourc.count
    }

    // MARK: - TableView delegate
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CountryCell = tableView.dequeueReusableCell(for: indexPath)

        guard let cellModel = viewModel?.datasource.value[indexPath.row] else { return UITableViewCell() }

        cell.setup(with: cellModel)

        return cell
    }

    // MARK: Private helpers
    private func setupTableView() {
        countryLisTableView = UITableView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
        countryLisTableView.delegate = self
        countryLisTableView.dataSource = self
        countryLisTableView.separatorStyle = .none
        countryLisTableView.registerCell(with: CountryCell.self)

        self.view.addSubview(countryLisTableView)
    }

    private func setupLoader() {
        activityIndicator = UIActivityIndicatorView(style: .medium)
        activityIndicator.startAnimating()
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(activityIndicator)

        activityIndicator.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        activityIndicator.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
    }

    private func showLoading(_ show: Bool) {
        show ? activityIndicator.startAnimating() : activityIndicator.stopAnimating()
    }
}

