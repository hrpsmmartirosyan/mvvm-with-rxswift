//
//  CountryCellPresentationModel.swift
//  MVVM with RxSwift
//
//  Created by hripsimem on 12/2/20.
//  Copyright © 2020 hripsimem. All rights reserved.
//

import Foundation

struct CountryCellPresentationModel {
    let name: String?
    let description: String?
}
