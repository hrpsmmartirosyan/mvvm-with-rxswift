//
//  CountryCell.swift
//  MVVM with RxSwift
//
//  Created by hripsimem on 12/3/20.
//  Copyright © 2020 hripsimem. All rights reserved.
//

import UIKit

final class CountryCell: UITableViewCell {
    @IBOutlet private weak var countryNameLabel: UILabel!
    @IBOutlet private weak var countryDescLabel: UILabel!

    func setup(with model: CountryCellPresentationModel) {
        self.countryNameLabel.text = model.name
        self.countryDescLabel.text = model.description
    }
}
