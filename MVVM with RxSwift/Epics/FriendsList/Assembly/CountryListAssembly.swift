//
//  CountryListAssembly.swift
//  MVVM with RxSwift
//
//  Created by hripsimem on 12/3/20.
//  Copyright © 2020 hripsimem. All rights reserved.
//

import Swinject
import SwinjectStoryboard
import SwinjectAutoregistration

final class CountryListAssembly: BaseAssembly {
    let countryListAssemblies: [Assembly] = [CountryListServiceAssembly()]

    override func assemble(container: Container) {
        self.countryListAssemblies.forEach({ $0.assemble(container: container) })
        
        container.autoregister(CountryListInputs.self, initializer: CountryListInputs.init)
        container.autoregister(CountryListViewModel.self, initializer: CountryListViewModel.init)

        container.register(CountryListViewController.self) { r in
            let controller = CountryListViewController()

            controller.viewModel = r.resolve(CountryListViewModel.self)
            return controller
        }
    }
}

