//
//  CountryListServiceAssembly.swift
//  MVVM with RxSwift
//
//  Created by hripsimem on 12/3/20.
//  Copyright © 2020 hripsimem. All rights reserved.
//

import Foundation
import Swinject

open class CountryListServiceAssembly: Assembly {
    public init() {}

    open func assemble(container: Container) {
        container.register(CountryListServicing.self) { r in
            
            container.autoregister(NetworkManager.self, initializer: NetworkManager.init)
            container.autoregister(Parser.self, initializer: Parser.init)

            return CountryListWebService(with: r.resolve(NetworkManager.self)!, parser: r.resolve((Parser.self))!)
        }
    }
}
